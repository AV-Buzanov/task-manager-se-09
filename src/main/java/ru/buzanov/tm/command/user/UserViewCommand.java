package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class UserViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View user information";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[VIEW USER]");
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        Printer.printInfo("[LOGIN]");
        Printer.print(serviceLocator.getUserService().getCurrentUser().getLogin());
        Printer.printInfo("[NAME]");
        Printer.print(serviceLocator.getUserService().getCurrentUser().getName());
        Printer.printInfo("[ROLE]");
        Printer.print(serviceLocator.getUserService().getCurrentUser().getRoleType().displayName());
        Printer.printInfo("[PROJECTS COUNT]");
        Printer.print(String.valueOf(Objects.requireNonNull(serviceLocator.getProjectService().findAll(userId)).size()));
        Printer.printInfo("[TASKS COUNT]");
        Printer.print(String.valueOf(Objects.requireNonNull(serviceLocator.getTaskService().findAll(userId)).size()));
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
