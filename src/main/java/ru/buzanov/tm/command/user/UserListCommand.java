package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.util.Printer;

public class UserListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-list";
    }

    @NotNull
    @Override
    public String description() {
        return "View user list (for admin only)";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[USER LIST]");
        for (User user:serviceLocator.getUserService().findAll()){
            Printer.printInfo(false,"[LOGIN] ");
            Printer.print(false,user.getLogin());
            Printer.printInfo(false," [NAME] ");
            Printer.print(false,user.getName());
            Printer.printInfo(false," [ROLE] ");
            Printer.print(user.getRoleType().displayName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }

    @Override
    public boolean isRoleAllow(RoleType role) {
        if (RoleType.USER.equals(role))
        return false;
        return super.isRoleAllow(role);
    }
}
