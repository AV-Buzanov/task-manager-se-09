package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.enumerated.RoleType;
import ru.buzanov.tm.util.Printer;

public class UserRegisterCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "register";
    }

    @NotNull
    @Override
    public String description() {
        return "User registration";
    }

    @Override
    public void execute() throws Exception {
        String stringBuf;
        Printer.printInfo("[REGISTRATION]");
        Printer.printInfo("[ENTER LOGIN]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty()) {
            Printer.printError("Login can't be empty");
            return;
        }
        if (serviceLocator.getUserService().isLoginExist(stringBuf)) {
            Printer.printError("This login already exist");
            return;
        }
        User user = new User();
        user.setLogin(stringBuf);
        Printer.printInfo("[ENTER PASS]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty() || stringBuf.length() < 6) {
            Printer.printError("Pass can't be empty and less then 6 symbols");
            return;
        }
        Printer.printInfo("[REPEAT PASS]");

        if (!stringBuf.equals(reader.readLine())) {
            Printer.printError("Invalid pass");
            return;
        }
        user.setPasswordHash(stringBuf);
        Printer.printInfo("[ENTER YOUR NAME]");
        stringBuf = reader.readLine();
        user.setName(stringBuf);
        user.setRoleType(RoleType.USER);
        serviceLocator.getUserService().load(user);
        Printer.printInfo("[Hello, " , user.getName() , ", now type auth to log in]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
