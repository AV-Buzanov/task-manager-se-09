package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.util.Printer;

public class UserLogOutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "log-out";
    }

    @NotNull
    @Override
    public String description() {
        return "User log-out";
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getUserService().setCurrentUser(null);
        Printer.printInfo("[YOU ARE UNAUTHORIZED NOW]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
