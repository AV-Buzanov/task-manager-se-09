package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class UserAuthCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "auth";
    }

    @NotNull
    @Override
    public String description() {
        return "User authentication";
    }

    @Override
    public void execute() throws Exception {
        String stringBuf;
        Printer.printInfo("[AUTHORISATION]");
        Printer.printInfo("[ENTER LOGIN]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty()) {
            Printer.printError("Login can't be empty");
            return;
        }
        if (!serviceLocator.getUserService().isLoginExist(stringBuf)) {
            Printer.printError("User doesn't exist, register please");
            return;
        }
        User user = serviceLocator.getUserService().findByLogin(stringBuf);
        Printer.printInfo("[ENTER PASS]");
        stringBuf = reader.readLine();

        if (!serviceLocator.getUserService().isPassCorrect(Objects.requireNonNull(user).getLogin(), stringBuf)) {
            Printer.printError("Invalid pass");
            return;
        }
        serviceLocator.getUserService().setCurrentUser(user);
        Printer.printInfo("[HELLO, " + user.getName() + ", NICE TO SEE YOU!]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
