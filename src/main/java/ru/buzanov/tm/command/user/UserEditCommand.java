package ru.buzanov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.User;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class UserEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit user data";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[EDIT USER DATA]");
        Printer.printInfo("[ENTER PASS TO EDIT]");
        String stringBuf;
        User user = serviceLocator.getUserService().getCurrentUser();
        stringBuf = reader.readLine();
        if (!serviceLocator.getUserService().isPassCorrect(Objects.requireNonNull(user).getLogin(), stringBuf)) {
            Printer.printError("Wrong pass");
            return;
        }
        Printer.printInfo("[LOGIN]");
        Printer.print(user.getLogin());
        Printer.printInfo("[ENTER NEW LOGIN]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty()) {
            if (serviceLocator.getUserService().isLoginExist(stringBuf)) {
                Printer.printError("User with this login already exist.");
                return;
            }
            user.setLogin(stringBuf);
        }
        Printer.printInfo("[NAME]");
        Printer.print(user.getName());
        Printer.printInfo("[ENTER NEW NAME]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty()) {
            user.setName(stringBuf);
        }
        Printer.printInfo("[ENTER NEW PASS IF NECESSARY]");
        stringBuf = reader.readLine();

        if (!stringBuf.isEmpty()) {
            if (stringBuf.length() > 6) {
                Printer.printInfo("[REPEAT NEW PASS]");
                if (stringBuf.equals(reader.readLine())) {
                    user.setPasswordHash(stringBuf);
                } else
                    Printer.printError("Pass not match");
            } else
                Printer.printError("Pass can't be less then 6 symbols");
        }
        Printer.printInfo("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
