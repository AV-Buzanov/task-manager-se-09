package ru.buzanov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.util.Printer;

public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() throws Exception {
        for (AbstractCommand command : serviceLocator.getCommands()) {
            Printer.printInfo(false, command.command());
            Printer.print(" : ", command.description());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
