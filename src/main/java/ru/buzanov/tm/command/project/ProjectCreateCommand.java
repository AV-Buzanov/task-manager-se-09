package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        Project project = new Project();
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        String stringBuf;
        Printer.printInfo("[PROJECT CREATE]");
        Printer.printInfo("[ENTER NAME]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty()) {
            Printer.printError("Project name can't be empty");
            return;
        }

        if (serviceLocator.getProjectService().isNameExist(userId, stringBuf)) {
            Printer.printError("Project with this name already exist.");
            return;
        }
        project.setName(stringBuf);
        Printer.printInfo("[ENTER PROJECT START DATE ", FormatConst.DATE_FORMAT , " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setStartDate(DateUtil.dateFormat().parse(stringBuf));
        Printer.printInfo("[ENTER PROJECT END DATE " , FormatConst.DATE_FORMAT , " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setEndDate(DateUtil.dateFormat().parse(stringBuf));
        Printer.printInfo("[ENTER DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setDescription(stringBuf);
        project.setUserId(userId);
        serviceLocator.getProjectService().load(project);

        Printer.printInfo("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
