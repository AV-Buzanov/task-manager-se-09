package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class ProjectSortListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-list-sorted";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects sorted";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[SORTED PROJECT LIST]");
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        List<Project> list = (List<Project>) serviceLocator.getProjectService().findAll(userId);

        Printer.printInfo("[SORT BY]");
        Printer.print("1 : By creation");
        Printer.print("2 : By name");
        Printer.print("3 : By start date");
        Printer.print("4 : By end date");
        Printer.print("5 : By tasks");
        Printer.print("6 : By status");
        String comp = reader.readLine();
        Printer.printInfo("[DIRECTION]");
        Printer.print("1 : Rising");
        Printer.print("2 : Falling");
        boolean dir = true;
        if ("2".equals(reader.readLine()))
            dir = false;


        switch (comp) {
            case ("1"):
                if (dir)
                    break;
                else
                    Collections.reverse(Objects.requireNonNull(list));
                break;
            case ("2"):
                Collections.sort(Objects.requireNonNull(list), serviceLocator.getProjectService().getNameComparator(dir));
                break;
            case ("3"):
                Collections.sort(Objects.requireNonNull(list), serviceLocator.getProjectService().getStartDateComparator(dir));
                break;
            case ("4"):
                Collections.sort(Objects.requireNonNull(list), serviceLocator.getProjectService().getEndDateComparator(dir));
                break;
            case ("5"):
                if (dir)
                    Collections.sort(Objects.requireNonNull(list), new Comparator<Project>() {
                        @Override
                        public int compare(Project o1, Project o2) {
                            int o1size = Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(o1.getId())).size();
                            int o2size = Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(o2.getId())).size();
                            return Integer.compare(o1size, o2size);
                        }
                    });
                else
                    Collections.sort(Objects.requireNonNull(list), new Comparator<Project>() {
                        @Override
                        public int compare(Project o1, Project o2) {
                            int o1size = Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(o1.getId())).size();
                            int o2size = Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(o2.getId())).size();
                            return Integer.compare(o1size, o2size) * (-1);
                        }
                    });
                break;
            case ("6"):
                Collections.sort(Objects.requireNonNull(list), serviceLocator.getProjectService().getStatusComparator(dir));
                break;
            default:
                break;
        }

        for (Project project : Objects.requireNonNull(list)) {
            Printer.printInfo(false, "[NAME] ");
            Printer.print(false, project.getName());
            Printer.printInfo(false, " [CREATE DATE] ");
            if (project.getCreateDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(project.getCreateDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);

            Printer.printInfo(false, " [START DATE] ");
            if (project.getStartDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(project.getStartDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);
            Printer.printInfo(false, " [END DATE] ");
            if (project.getEndDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(project.getEndDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);
            Printer.printInfo(false, " [TASKS] ");
            Printer.print(false, String.valueOf(Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(project.getId())).size()));
            Printer.printInfo(false, " [STATUS] ");
            Printer.print(project.getStatus().displayName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
