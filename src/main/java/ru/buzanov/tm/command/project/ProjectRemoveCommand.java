package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected project with connected tasks.";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[CHOOSE PROJECT TO REMOVE]");
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        Printer.print(serviceLocator.getProjectService().getList(userId));
        String idBuf = serviceLocator.getProjectService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        serviceLocator.getProjectService().remove(userId, Objects.requireNonNull(idBuf));
        serviceLocator.getTaskService().removeByProjectId(userId, idBuf);

        Printer.printInfo("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
