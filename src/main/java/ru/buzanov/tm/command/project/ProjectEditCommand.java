package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class ProjectEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit project";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[CHOOSE PROJECT TO EDIT]");
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        Printer.print(serviceLocator.getProjectService().getList(userId));
        String stringBuf;
        stringBuf = serviceLocator.getProjectService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        Project project = Objects.requireNonNull(serviceLocator.getProjectService().findOne(userId, stringBuf));
        Printer.printInfo("[NAME]");
        Printer.print(project.getName());
        Printer.printInfo("[ENTER NEW NAME]");
        stringBuf = reader.readLine();

        if (serviceLocator.getProjectService().isNameExist(userId, stringBuf)) {
            Printer.printError("Project with this name already exist.");
            return;
        }
        if (!stringBuf.isEmpty())
            project.setName(stringBuf);

        Printer.printInfo("[START DATE]");
        if (project.getStartDate() != null)
            Printer.print(DateUtil.dateFormat().format(project.getStartDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[ENTER NEW START DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setStartDate(DateUtil.dateFormat().parse(stringBuf));
        Printer.printInfo("[END DATE]");
        if (project.getEndDate() != null)
            Printer.print(DateUtil.dateFormat().format(project.getEndDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[ENTER NEW END DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setEndDate(DateUtil.dateFormat().parse(stringBuf));
        Printer.printInfo("[DESCRIPTION]");
        if (project.getDescription() != null)
            Printer.print(project.getDescription());
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[ENTER NEW DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            project.setDescription(stringBuf);
        Printer.printInfo("[STATUS]");
        Printer.print(project.getStatus().displayName());
        Printer.printInfo("[NEW STATUS]");
        int i = 1;
        for (Status st : Status.values()) {
            Printer.print(String.valueOf(i), ": ", st.displayName());
            i++;
        }
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty()) {
            i = 1;
            for (Status st : Status.values()) {
                if (i == Integer.parseInt(stringBuf))
                    project.setStatus(st);
                i++;
            }
        }

        serviceLocator.getProjectService().merge(userId, project.getId(), project);

        Printer.printInfo("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
