package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class ProjectClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {

        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        for (Project project : Objects.requireNonNull(serviceLocator.getProjectService().findAll(userId)))
            serviceLocator.getTaskService().removeByProjectId(userId, project.getId());
        serviceLocator.getProjectService().removeAll(userId);

        Printer.printInfo("[ALL PROJECTS REMOVED WITH CONNECTED TASKS]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
