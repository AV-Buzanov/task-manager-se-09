package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectFindCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-find";
    }

    @NotNull
    @Override
    public String description() {
        return "Find projects by name or description.";
    }

    @Override
    public void execute() throws Exception {
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        List<Project> list = new ArrayList<>();

        Printer.printInfo("[FIND BY]");
        Printer.print("1 : Name");
        Printer.print("2 : Description");
        String s = reader.readLine();
        switch (s) {
            case ("1"):
                Printer.printInfo("[ENTER NAME]");
                list = (List<Project>) serviceLocator.getProjectService().findByName(userId, reader.readLine());
                break;
            case ("2"):
                Printer.printInfo("[ENTER DESCRIPTION]");
                list = (List<Project>) serviceLocator.getProjectService().findByDescription(userId, reader.readLine());
                break;
            default:
                break;
        }
        assert list != null;
        if (list.isEmpty())
            Printer.print(FormatConst.EMPTY_FIELD);
        else {
            for (Project project : list) {
                Printer.printInfo(false, "[NAME] ");
                Printer.print(false, project.getName());
                Printer.printInfo(false, " [CREATE DATE] ");
                if (project.getCreateDate() != null)
                    Printer.print(false, DateUtil.dateFormat().format(project.getCreateDate()));
                else
                    Printer.print(false, FormatConst.EMPTY_FIELD);

                Printer.printInfo(false, " [START DATE] ");
                if (project.getStartDate() != null)
                    Printer.print(false, DateUtil.dateFormat().format(project.getStartDate()));
                else
                    Printer.print(false, FormatConst.EMPTY_FIELD);
                Printer.printInfo(false, " [END DATE] ");
                if (project.getEndDate() != null)
                    Printer.print(false, DateUtil.dateFormat().format(project.getEndDate()));
                else
                    Printer.print(false, FormatConst.EMPTY_FIELD);
                Printer.printInfo(false, " [TASKS] ");
                Printer.print(false, String.valueOf(Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(project.getId())).size()));
                Printer.printInfo(false, " [STATUS] ");
                Printer.print(project.getStatus().displayName());
            }
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
