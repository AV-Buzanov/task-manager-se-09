package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[PROJECT LIST]");
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        for (Project project : Objects.requireNonNull(serviceLocator.getProjectService().findAll(userId))) {
            Printer.printInfo(false, "[NAME] ");
            Printer.print(false, project.getName());
            Printer.printInfo(false, " [CREATE DATE] ");
            if (project.getCreateDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(project.getCreateDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);

            Printer.printInfo(false, " [START DATE] ");
            if (project.getStartDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(project.getStartDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);
            Printer.printInfo(false, " [END DATE] ");
            if (project.getEndDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(project.getEndDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);
            Printer.printInfo(false, " [TASKS] ");
            Printer.print(false, String.valueOf(Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(project.getId())).size()));
            Printer.printInfo(false, " [STATUS] ");
            Printer.print(project.getStatus().displayName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
