package ru.buzanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class ProjectViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View project information";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[CHOOSE PROJECT TO VIEW]");

        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        Printer.print(serviceLocator.getProjectService().getList(userId));
        String idBuf = serviceLocator.getProjectService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        Project projectBuf = serviceLocator.getProjectService().findOne(userId, idBuf);
        Printer.printInfo("[NAME] ");
        Printer.print(Objects.requireNonNull(projectBuf).getName());
        Printer.printInfo("[CREATE DATE] ");
        if (projectBuf.getCreateDate() != null)
            Printer.print(DateUtil.dateFormat().format(projectBuf.getCreateDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[START DATE] ");
        if (projectBuf.getStartDate() != null)
            Printer.print(DateUtil.dateFormat().format(projectBuf.getStartDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[END DATE] ");
        if (projectBuf.getEndDate() != null)
            Printer.print(DateUtil.dateFormat().format(projectBuf.getEndDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[DESCRIPTION] ");
        if (projectBuf.getDescription() != null)
            Printer.print(projectBuf.getDescription());
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[STATUS] ");
        if (projectBuf.getStatus() != null)
            Printer.print(projectBuf.getStatus().displayName());
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[TASKS(", String.valueOf(Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(userId, idBuf)).size()), ")]");

        if (Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(userId, idBuf)).isEmpty()) {
            Printer.print(FormatConst.EMPTY_FIELD);
            return;
        }
        for (Task task : Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(userId, idBuf))) {
            Printer.print(task.getName(), " : ", task.getStatus().displayName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
