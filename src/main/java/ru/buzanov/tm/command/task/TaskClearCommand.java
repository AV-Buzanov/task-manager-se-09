package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class TaskClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        serviceLocator.getTaskService().removeAll(userId);
        Printer.printInfo("[ALL TASKS REMOVED]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
