package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Project;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskFindNameCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-find";
    }

    @NotNull
    @Override
    public String description() {
        return "Find tasks by name, description or project name.";
    }

    @Override
    public void execute() throws Exception {
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        List<Task> list = new ArrayList<>();

        Printer.printInfo("[FIND BY]");
        Printer.print("1 : Name");
        Printer.print("2 : Description");
        Printer.print("3 : Project name");
        String s = reader.readLine();
        switch (s) {
            case ("1"):
                Printer.printInfo("[ENTER NAME]");
                list = (List<Task>) serviceLocator.getTaskService().findByName(userId, reader.readLine());
                break;
            case ("2"):
                Printer.printInfo("[ENTER DESCRIPTION]");
                list = (List<Task>) serviceLocator.getTaskService().findByDescription(userId, reader.readLine());
                break;
            case ("3"):
                Printer.printInfo("[ENTER PROJECT NAME]");
                String projectname = reader.readLine();
                for (Project project : Objects.requireNonNull(serviceLocator.getProjectService().findByName(userId, projectname)))
                    list.addAll(Objects.requireNonNull(serviceLocator.getTaskService().findByProjectId(userId, project.getId())));
                break;
            default:
                break;
        }
        if (Objects.requireNonNull(list).isEmpty())
            Printer.print(FormatConst.EMPTY_FIELD);
        else {
            for (Task task : list) {
                Printer.printInfo(false, "[NAME] ");
                Printer.print(false, task.getName());
                Printer.printInfo(false, " [CREATE DATE] ");
                if (task.getCreateDate() != null)
                    Printer.print(false, DateUtil.dateFormat().format(task.getCreateDate()));
                else
                    Printer.print(false, FormatConst.EMPTY_FIELD);

                Printer.printInfo(false, " [START DATE] ");
                if (task.getStartDate() != null)
                    Printer.print(false, DateUtil.dateFormat().format(task.getStartDate()));
                else
                    Printer.print(false, FormatConst.EMPTY_FIELD);
                Printer.printInfo(false, " [END DATE] ");
                if (task.getEndDate() != null)
                    Printer.print(false, DateUtil.dateFormat().format(task.getEndDate()));
                else
                    Printer.print(false, FormatConst.EMPTY_FIELD);
                Printer.printInfo(false, " [PROJECT] ");
                if (task.getProjectId() != null)
                    Printer.print(false, Objects.requireNonNull(serviceLocator.getProjectService().findOne(userId, task.getProjectId())).getName());
                else
                    Printer.print(false, FormatConst.EMPTY_FIELD);
                Printer.printInfo(false, " [STATUS] ");
                Printer.print(task.getStatus().displayName());
            }
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
