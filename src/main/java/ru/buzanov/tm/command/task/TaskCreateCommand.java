package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class TaskCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        Task task = new Task();
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        String stringBuf;
        Printer.printInfo("[TASK CREATE]");
        Printer.printInfo("[ENTER NAME]");
        stringBuf = reader.readLine();
        if (stringBuf.isEmpty()) {
            Printer.printError("Task name can't be empty");
            return;
        }
        if (serviceLocator.getTaskService().isNameExist(userId, stringBuf)) {
            Printer.printError("Task with this name already exist.");
            return;
        }
        task.setName(stringBuf);
        Printer.printInfo("[ENTER TASK START DATE " , FormatConst.DATE_FORMAT , " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setStartDate(DateUtil.dateFormat().parse(stringBuf));
        Printer.printInfo("[ENTER TASK END DATE " , FormatConst.DATE_FORMAT , " ]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setEndDate(DateUtil.dateFormat().parse(stringBuf));
        Printer.printInfo("[ENTER DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setDescription(stringBuf);

        if (!Objects.requireNonNull(serviceLocator.getProjectService().findAll(userId)).isEmpty()) {
            Printer.printInfo("[CHOOSE PROJECT]");
            Printer.print(serviceLocator.getProjectService().getList(userId));
            stringBuf = reader.readLine();
            if (!stringBuf.isEmpty()) {
                task.setProjectId(serviceLocator.getProjectService().getIdByCount(userId, Integer.parseInt(stringBuf)));
            }
        }
        task.setUserId(userId);
        serviceLocator.getTaskService().load(task);
        Printer.printInfo("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
