package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class TaskListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[TASK LIST]");
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        for (Task task : Objects.requireNonNull(serviceLocator.getTaskService().findAll(userId))) {
            Printer.printInfo(false, "[NAME] ");
            Printer.print(false, task.getName());
            Printer.printInfo(false, " [CREATE DATE] ");
            if (task.getCreateDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(task.getCreateDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);

            Printer.printInfo(false, " [START DATE] ");
            if (task.getStartDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(task.getStartDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);
            Printer.printInfo(false, " [END DATE] ");
            if (task.getEndDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(task.getEndDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);
            Printer.printInfo(false, " [PROJECT] ");
            if (task.getProjectId() != null)
                Printer.print(false, Objects.requireNonNull(serviceLocator.getProjectService().findOne(userId, task.getProjectId())).getName());
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);
            Printer.printInfo(false, " [STATUS] ");
            Printer.print(task.getStatus().displayName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
