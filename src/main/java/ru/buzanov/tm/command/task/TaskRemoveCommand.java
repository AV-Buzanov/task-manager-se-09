package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[CHOOSE TASK TO REMOVE]");
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        Printer.print(serviceLocator.getTaskService().getList(userId));
        String idBuf = serviceLocator.getTaskService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        serviceLocator.getTaskService().remove(userId, Objects.requireNonNull(idBuf));
        Printer.printInfo("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
