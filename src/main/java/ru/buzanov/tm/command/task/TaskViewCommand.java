package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class TaskViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-view";
    }

    @NotNull
    @Override
    public String description() {
        return "View task information";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[CHOOSE TASK TO VIEW]");
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        Printer.print(serviceLocator.getTaskService().getList(userId));
        String idBuf = serviceLocator.getTaskService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        Task task = serviceLocator.getTaskService().findOne(userId, idBuf);
        Printer.printInfo("[NAME]");
        Printer.print(Objects.requireNonNull(task).getName());
        Printer.printInfo("[CREATE DATE] ");
        if (task.getCreateDate() != null)
            Printer.print(DateUtil.dateFormat().format(task.getCreateDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[START DATE]");
        if (task.getStartDate() != null)
            Printer.print(DateUtil.dateFormat().format(task.getStartDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[END DATE]");
        if (task.getEndDate() != null)
            Printer.print(DateUtil.dateFormat().format(task.getEndDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[DESCRIPTION]");
        if (task.getDescription() != null)
            Printer.print(task.getDescription());
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[DESCRIPTION]");
        if (task.getStatus() != null)
            Printer.print(task.getStatus().displayName());
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[PROJECT]");
        if (task.getProjectId() != null)
            Printer.print(Objects.requireNonNull(serviceLocator.getProjectService().findOne(userId, task.getProjectId())).getName());
        else
            Printer.print(FormatConst.EMPTY_FIELD);
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
