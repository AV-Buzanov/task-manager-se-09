package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.enumerated.Status;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Objects;

public class TaskEditCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit task";
    }

    @Override
    public void execute() throws Exception {
        String stringBuf;
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        Printer.printInfo("[CHOOSE TASK TO EDIT]");

        Printer.print(serviceLocator.getTaskService().getList(userId));
        stringBuf = serviceLocator.getTaskService().getIdByCount(userId, Integer.parseInt(reader.readLine()));
        Task task = Objects.requireNonNull(serviceLocator.getTaskService().findOne(userId, stringBuf));
        Printer.printInfo("[NAME]");
        Printer.print(task.getName());
        Printer.printInfo("[ENTER NEW NAME]");
        stringBuf = reader.readLine();
        if (serviceLocator.getTaskService().isNameExist(userId, stringBuf)) {
            Printer.printError("Task with this name already exist.");
            return;
        }
        if (!stringBuf.isEmpty())
            task.setName(stringBuf);
        Printer.printInfo("[START DATE]");
        if (task.getStartDate() != null)
            Printer.print(DateUtil.dateFormat().format(task.getStartDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[ENTER NEW START DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setStartDate(DateUtil.dateFormat().parse(stringBuf));
        Printer.printInfo("[END DATE]");
        if (task.getEndDate() != null)
            Printer.print(DateUtil.dateFormat().format(task.getEndDate()));
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[ENTER NEW END DATE]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setEndDate(DateUtil.dateFormat().parse(stringBuf));
        Printer.printInfo("[DESCRIPTION]");
        if (task.getDescription() != null)
            Printer.print(task.getDescription());
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[ENTER NEW DESCRIPTION]");
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty())
            task.setDescription(stringBuf);
        Printer.printInfo("[STATUS]");
        Printer.print(task.getStatus().displayName());
        Printer.printInfo("[NEW STATUS]");
        int i = 1;
        for (Status st : Status.values()) {
            Printer.print(String.valueOf(i), ": ", st.displayName());
            i++;
        }
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty()) {
            i = 1;
            for (Status st : Status.values()) {
                if (i == Integer.parseInt(stringBuf))
                    task.setStatus(st);
                i++;
            }
        }
        Printer.printInfo("[PROJECT]");
        if (task.getProjectId() != null)

            Printer.print(Objects.requireNonNull(serviceLocator.getProjectService().findOne(userId, task.getProjectId())).getName());
        else
            Printer.print(FormatConst.EMPTY_FIELD);
        Printer.printInfo("[CHOOSE NEW PROJECT, WRITE 0 TO REMOVE]");
        Printer.print(serviceLocator.getProjectService().getList(userId));
        stringBuf = reader.readLine();
        if (!stringBuf.isEmpty()) {
            if ("0".equals(stringBuf))
                stringBuf = null;
            else stringBuf = serviceLocator.getProjectService().getIdByCount(userId, Integer.parseInt(stringBuf));
            task.setProjectId(stringBuf);
        }
        serviceLocator.getTaskService().merge(userId, task.getId(), task);
        Printer.printInfo("[OK]");
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
