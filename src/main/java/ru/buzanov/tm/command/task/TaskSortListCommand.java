package ru.buzanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.command.AbstractCommand;
import ru.buzanov.tm.constant.FormatConst;
import ru.buzanov.tm.entity.Task;
import ru.buzanov.tm.util.DateUtil;
import ru.buzanov.tm.util.Printer;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class TaskSortListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "task-list-sorted";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks sorted";
    }

    @Override
    public void execute() throws Exception {
        Printer.printInfo("[SORTED TASK LIST]");
        String userId = Objects.requireNonNull(serviceLocator.getUserService().getCurrentUser()).getId();
        List<Task> list = (List<Task>) serviceLocator.getTaskService().findAll(userId);

        Printer.printInfo("[SORT BY]");
        Printer.print("1 : By creation");
        Printer.print("2 : By name");
        Printer.print("3 : By start date");
        Printer.print("4 : By end date");
        Printer.print("5 : By status");
        String comp = reader.readLine();
        Printer.printInfo("[DIRECTION]");
        Printer.print("1 : Rising");
        Printer.print("2 : Falling");
        boolean dir = true;
        if ("2".equals(reader.readLine()))
            dir = false;

        switch (comp) {
            case ("1"):
                if (dir)
                    break;
                else
                    Collections.reverse(Objects.requireNonNull(list));
                break;
            case ("2"):
                Collections.sort(Objects.requireNonNull(list), serviceLocator.getTaskService().getNameComparator(dir));
                break;
            case ("3"):
                Collections.sort(Objects.requireNonNull(list), serviceLocator.getTaskService().getStartDateComparator(dir));
                break;
            case ("4"):
                Collections.sort(Objects.requireNonNull(list), serviceLocator.getTaskService().getEndDateComparator(dir));
                break;
            case ("5"):
                Collections.sort(Objects.requireNonNull(list), serviceLocator.getTaskService().getStatusComparator(dir));
                break;
            default:
                break;
        }

        for (Task task : Objects.requireNonNull(list)) {
            Printer.printInfo(false, "[NAME] ");
            Printer.print(false, task.getName());
            Printer.printInfo(false, " [CREATE DATE] ");
            if (task.getCreateDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(task.getCreateDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);

            Printer.printInfo(false, " [START DATE] ");
            if (task.getStartDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(task.getStartDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);
            Printer.printInfo(false, " [END DATE] ");
            if (task.getEndDate() != null)
                Printer.print(false, DateUtil.dateFormat().format(task.getEndDate()));
            else
                Printer.print(false, FormatConst.EMPTY_FIELD);
            Printer.printInfo(false, " [STATUS] ");
            Printer.print(task.getStatus().displayName());
        }
    }

    @Override
    public boolean isSecure() throws Exception {
        return true;
    }
}
