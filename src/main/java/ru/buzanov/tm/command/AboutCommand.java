package ru.buzanov.tm.command;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.util.Printer;

public class AboutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Information about application";
    }

    @Override
    public void execute() throws Exception {

        Printer.printInfo("Task-Manager ver.SE-09, " , Manifests.read("buildNumber"));
        Printer.printInfo("Developer: " , Manifests.read("developer"));
    }

    @Override
    public boolean isSecure() throws Exception {
        return false;
    }
}
