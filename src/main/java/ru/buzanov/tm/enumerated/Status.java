package ru.buzanov.tm.enumerated;

public enum Status {
    PLANNED("Запланированно"),
    IN_PROGRESS("В процессе"),
    DONE("Завершено");

    private String name;

    Status(String name) {
        this.name = name;
    }

    public String displayName() {
        return name;
    }
}
