package ru.buzanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ISubjectRepository<T> extends IRepository<T> {

    @NotNull Collection<T> findAll(@NotNull String userId);

    @Nullable T findOne(@NotNull String userId, @NotNull String id);

    boolean isNameExist(@NotNull String userId, @NotNull String name);

    @NotNull String getList();

    @NotNull String getList(@NotNull String userId);

    @Nullable String getIdByCount(int count);

    @Nullable String getIdByCount(String userId, int count);

    void merge(@NotNull String userId, @NotNull String id, @NotNull T project);

    @Nullable T remove(@NotNull String userId, @NotNull String id);

    void removeAll(@NotNull String userId);

    @NotNull Collection<T> findByDescription(@NotNull final String userId, @NotNull final String desc);

    @NotNull Collection<T> findByName(@NotNull final String userId, @NotNull final String name);
}
