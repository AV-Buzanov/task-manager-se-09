package ru.buzanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IService<T> {

    @NotNull Collection<T> findAll();

    @Nullable T findOne(@Nullable String id);

    void merge(@Nullable String id, @Nullable T entity);

    @Nullable T remove(@Nullable String id);

    void removeAll();

    @Nullable T load(@Nullable T entity);
}
