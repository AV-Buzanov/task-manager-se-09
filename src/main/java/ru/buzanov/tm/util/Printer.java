package ru.buzanov.tm.util;

public class Printer {
    public static void print(final String... strings) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : strings) {
            stringBuilder.append(string);
        }
        System.out.println(stringBuilder.toString());
    }

    public static void print(final boolean nextLine, final String... strings) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : strings) {
            stringBuilder.append(string);
        }
        if (nextLine)
            System.out.println(stringBuilder.toString());
        else
            System.out.print(stringBuilder.toString());    }

    public static void printInfo(final String... strings) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append((char) 27).append("[32m");
        for (String string : strings) {
            stringBuilder.append(string);
        }
        stringBuilder.append((char) 27).append("[0m");
        System.out.println(stringBuilder.toString());
    }

    public static void printInfo(final boolean nextLine, final String... strings) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append((char) 27).append("[32m");
        for (String string : strings) {
            stringBuilder.append(string);
        }
        stringBuilder.append((char) 27).append("[0m");
        if (nextLine)
            System.out.println(stringBuilder.toString());
        else
            System.out.print(stringBuilder.toString());    }

    public static void printError(final String... strings) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append((char) 27).append("[31m");
        for (String string : strings) {
            stringBuilder.append(string);
        }
        stringBuilder.append((char) 27).append("[0m");
        System.out.println(stringBuilder.toString());
    }

    public static void printError(final boolean nextLine, final String... strings) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append((char) 27).append("[31m");
        for (String string : strings) {
            stringBuilder.append(string);
        }
        stringBuilder.append((char) 27).append("[0m");
        if (nextLine)
            System.out.println(stringBuilder.toString());
        else
            System.out.print(stringBuilder.toString());
    }
}
