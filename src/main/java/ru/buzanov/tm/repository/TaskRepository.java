package ru.buzanov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.buzanov.tm.api.repository.ITaskRepository;
import ru.buzanov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Collection;

@NoArgsConstructor
public class TaskRepository extends AbstractSubjectRepository<Task> implements ITaskRepository {

    public void merge(@NotNull final String id, @NotNull final Task task) {
        if (task.getName() != null)
            map.get(id).setName(task.getName());
        if (task.getStartDate() != null)
            map.get(id).setStartDate(task.getStartDate());
        if (task.getEndDate() != null)
            map.get(id).setEndDate(task.getEndDate());
        if (task.getDescription() != null)
            map.get(id).setDescription(task.getDescription());
        if (task.getUserId() != null)
            map.get(id).setUserId(task.getUserId());
        if (task.getStatus() != null)
            map.get(id).setStatus(task.getStatus());
    }

    public void merge(@NotNull final String userId, @NotNull final String id, @NotNull final Task task) {
        if (!userId.equals(map.get(id).getUserId()))
            return;
        if (task.getName() != null)
            map.get(id).setName(task.getName());
        if (task.getStartDate() != null)
            map.get(id).setStartDate(task.getStartDate());
        if (task.getEndDate() != null)
            map.get(id).setEndDate(task.getEndDate());
        if (task.getDescription() != null)
            map.get(id).setDescription(task.getDescription());
        if (task.getUserId() != null)
            map.get(id).setUserId(task.getUserId());
        if (task.getStatus() != null)
            map.get(id).setStatus(task.getStatus());
    }

    @NotNull
    public Collection<Task> findByProjectId(@NotNull final String projectId) {
        Collection<Task> list = new ArrayList<>();
        for (Task task : findAll()) {
            if (projectId.equals(task.getProjectId()))
                list.add(task);
        }
        return list;
    }

    public void removeByProjectId(@NotNull final String projectId) {
        for (Task task : findAll()) {
            if (projectId.equals(task.getProjectId()))
                remove(task.getId());
        }
    }

    @NotNull
    public Collection<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        Collection<Task> list = new ArrayList<>();
        for (Task task : findAll(userId)) {
            if (projectId.equals(task.getProjectId()))
                list.add(task);
        }
        return list;
    }

    public void removeByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        for (Task task : findAll(userId)) {
            if (projectId.equals(task.getProjectId()))
                remove(task.getId());
        }
    }
}
