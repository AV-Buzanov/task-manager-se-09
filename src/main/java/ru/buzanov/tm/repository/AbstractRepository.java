package ru.buzanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.IRepository;
import ru.buzanov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    final Map<String, T> map = new LinkedHashMap<>();

    @Nullable
    public T load(@NotNull final T entity) {
        return map.put(entity.getId(), entity);
    }

    @NotNull
    public Collection<T> findAll() {
        return map.values();
    }

    @Nullable
    public T findOne(@NotNull final String id) {
        if (map.containsKey(id))
            return map.get(id);
        else return null;
    }

    public abstract void merge(@NotNull final String id, @NotNull final T entity);

    @Nullable
    public T remove(@NotNull final String id) {
        if (map.containsKey(id))
            return map.remove(id);
        else return null;
    }

    public void removeAll() {
        map.clear();
    }
}
