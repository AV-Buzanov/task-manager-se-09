package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.buzanov.tm.api.repository.ISubjectRepository;
import ru.buzanov.tm.api.service.ISubjectService;
import ru.buzanov.tm.entity.AbstractSubjectEntity;

import java.util.Collection;
import java.util.Comparator;

@NoArgsConstructor
public abstract class AbstractSubjectService<T extends AbstractSubjectEntity> extends AbstractService<T> implements ISubjectService<T> {
    private ISubjectRepository<T> abstractSubjectRepository;

    AbstractSubjectService(final ISubjectRepository<T> abstractRepository) {
        super(abstractRepository);
        this.abstractSubjectRepository = abstractRepository;
    }

    @Nullable
    public Collection<T> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.findAll(userId);
    }

    @Nullable
    public Collection<T> findByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty())
            return null;
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.findByName(userId, name);
    }

    @Nullable
    public Collection<T> findByDescription(@Nullable final String userId, @Nullable final String desc) {
        if (desc == null || desc.isEmpty())
            return null;
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.findByDescription(userId, desc);
    }

    @Nullable
    public T findOne(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        return abstractSubjectRepository.findOne(userId, id);
    }

    public boolean isNameExist(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty())
            return false;
        if (name == null || name.isEmpty())
            return false;
        return abstractSubjectRepository.isNameExist(userId, name);
    }

    @NotNull
    public String getList() {
        return abstractSubjectRepository.getList();
    }

    @Nullable
    public String getList(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.getList(userId);
    }

    @Nullable
    public String getIdByCount(final int count) {
        return abstractSubjectRepository.getIdByCount(count);
    }

    @Nullable
    public String getIdByCount(@Nullable final String userId, int count) {
        if (userId == null || userId.isEmpty())
            return null;
        return abstractSubjectRepository.getIdByCount(userId, count);
    }

    public void merge(@Nullable final String userId, @Nullable final String id, @Nullable final T entity) {
        if (userId == null || userId.isEmpty())
            return;
        if (id == null || id.isEmpty())
            return;
        if (entity == null)
            return;
        abstractSubjectRepository.merge(userId, id, entity);
    }

    @Nullable
    public T remove(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty())
            return null;
        if (id == null || id.isEmpty())
            return null;
        return abstractSubjectRepository.remove(userId, id);
    }

    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty())
            return;
        abstractSubjectRepository.removeAll(userId);
    }

    @NotNull
    public Comparator<T> getNameComparator(final boolean direction) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (direction)
                    return o1.getName().compareToIgnoreCase(o2.getName());
                return o1.getName().compareToIgnoreCase(o2.getName()) * (-1);
            }
        };
    }

    @NotNull
    public Comparator<T> getStartDateComparator(final boolean direction) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (o1.getStartDate() == null && o2.getStartDate() == null)
                    return 0;
                if (o1.getStartDate() == null && o2.getStartDate() != null)
                    return 1;
                if (o1.getStartDate() != null && o2.getStartDate() == null)
                    return -1;
                if (direction) {
                    assert o1.getStartDate() != null;
                    return o1.getStartDate().compareTo(o2.getStartDate());
                }
                assert o1.getStartDate() != null;
                return o1.getStartDate().compareTo(o2.getStartDate()) * (-1);
            }
        };
    }

    @NotNull
    public Comparator<T> getEndDateComparator(final boolean direction) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (o1.getEndDate() == null && o2.getEndDate() == null)
                    return 0;
                if (o1.getEndDate() == null && o2.getEndDate() != null)
                    return 1;
                if (o1.getEndDate() != null && o2.getEndDate() == null)
                    return -1;
                if (direction) {
                    assert o1.getEndDate() != null;
                    return o1.getEndDate().compareTo(o2.getEndDate());
                }
                assert o1.getEndDate() != null;
                return o1.getEndDate().compareTo(o2.getEndDate()) * (-1);
            }
        };
    }

    @NotNull
    public Comparator<T> getStatusComparator(final boolean direction) {
        return new Comparator<T>() {
            @Override
            public int compare(T o1, T o2) {
                if (direction)
                    return o1.getStatus().compareTo(o2.getStatus());
                return o1.getStatus().compareTo(o2.getStatus()) * (-1);
            }
        };
    }
}
