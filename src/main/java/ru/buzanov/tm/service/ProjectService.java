package ru.buzanov.tm.service;

import lombok.NoArgsConstructor;
import ru.buzanov.tm.api.repository.IProjectRepository;
import ru.buzanov.tm.api.service.IProjectService;
import ru.buzanov.tm.entity.Project;

@NoArgsConstructor
public class ProjectService extends AbstractSubjectService<Project> implements IProjectService {

    public ProjectService(IProjectRepository projectRepository) {
        super(projectRepository);
    }
}